# Upliving Q6 Mini LCD Video Projector

USB sticks moeten FAT32 geformatteerd zijn.

Video's uit Screenflow doen het niet. Wel als ze ge-her-encodeerd worden met mijn vaste FFMPEG 
instellingen:

```bash
ffmpeg -i input.mp4 -f mp4 -vcodec libx264 -pix_fmt yuv420p output.mp4
```

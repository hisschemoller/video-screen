# Philips PicoPix PPX 1230 beamer

* Beeldverhouding 4 x 3.
* AV ingang werkt goed.
* Zacht geluid uit intern luidsprkertje.
* VGA ingang voor stekker die er uit ziet als mini-HDMI.
* Resolutie 800 x 600 pixels (SVGA).

https://www.philips.nl/c-p/PPX1230_EU/picopix-zakprojector

![PicoPix](assets/images/philips-picopix-ppx-1230-1.jpg 'PicoPix')

![PicoPix](assets/images/philips-picopix-ppx-1230-2.jpg 'PicoPix')

# RPi Video Looper

## Installatie

* SD kaart aansluiten op de computer.
* Raspberry Pi Imager app starten.
* Instellingen:
  * Device: Raspberry Pi Zero
  * OS: Custom > Browse naar 'video_looper_2.8.zip'.
  * Storage: Generic USB SD Reader Media. (de SD kaart).
  * Next.
* Use OS Customisation? > Edit Settings.
  * Instellingen overnemen van Apple Keychain? > Ja.
  * Tab General:
    * Host: raspberrypi.local / user: pi / pass: raspberry
    * Configure Wireless LAN: WIFI gegevens invullen.
  * Tab Services:
    * Bij herinstallatie eerst de bestaande SSH key verwijderen:
      * `ssh-keygen -R {RPi-IP-Address}`, dus `ssh-keygen -R raspberrypi.local`.
    * Enable SSH aanvinken (public key is er al, van Keychain?)
  * Tab Options: Niks.
  * Save, Use OS Customisation? > Yes > Yes etc. tot het schrijven begint.
* Schrijven kan wel eens mislukken maar de volgende poging gaat het weer goed.
* SD kaart terug in de RPi.

### Eerste keer starten

* RPi met SD kaart en monitor aanzetten.
* Startup informatie, even een kleurvlak, daarna 'Insert USB drive'.
* Vanuit terminal `ssh pi@raspberrypi.local`.
* Mededeling:

```
The authenticity of host 'raspberrypi.local (fe80::e20a:786a:d58a:1b44%en0)' can't be established.
ED25519 key fingerprint is SHA256:6sKZZwe+czqn+vzRNNWzR0lUY04JkNEoT67IfNV+kEw.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

* Yes
* Mededeling:

```
Warning: Permanently added 'raspberrypi.local' (ED25519) to the list of known hosts.
Enter passphrase for key '/Users/wouter/.ssh/id_rsa':
```

* Passphrase zoals die opgeschreven staat.
* Daarna staat raspberrypi.local in /Users/wouter/.ssh/known_hosts
* Dan in Terminal:

```
Linux raspberrypi 5.10.103+ #1529 Tue Mar 8 12:19:18 GMT 2022 armv6l
---
inet 10.0.1.50 netmask 255.255.255.0 broadcast 10.0.1.255
 
       _     _            _                                       _      
__   _(_) __| | ___  ___ | | ___   ___  _ __   ___ _ __        __| | ___ 
\ \ / / |/ _` |/ _ \/ _ \| |/ _ \ / _ \| '_ \ / _ \ '__|      / _` |/ _ \
 \ V /| | (_| |  __/ (_) | | (_) | (_) | |_) |  __/ |     _  | (_| |  __/
  \_/ |_|\__,_|\___|\___/|_|\___/ \___/| .__/ \___|_|    (_)  \__,_|\___|
                                       |_|                               


Christian Sievers 2015-2024
www.videolooper.de
v2.8 (using pi_video_looper v1.0.17)
```

## Inloggen

* RPi met SD kaart en monitor aanzetten.
* Startup informatie, even een kleurvlak, daarna 'Insert USB drive'.
* Vanuit terminal `ssh pi@raspberrypi.local`.

Files on the card:

```
pi_video_looper/
  Adafruit_Video_Looper
  Adafruit_Video_Looper.egg-info
  assets
  build
  disable.sh
  dist
  enable.sh
  install.sh
  LICENSEE
  README.md
  reload.sh
  remount_rw_usbkey.sh
  setup.py
video/
```

## Instellingen aanpassen, video op SD-kaart

SD-kaart op de Macbook aansluiten. Verschijnt als 'boot'.\
`video_looper.ini` voor de instellingen. Mijn veranderingen:

```
file_reader = directory
```

De video locatie laat ik op `path = /home/pi/video`.

Met de SD-kaart op een Linux computer is ook `rootfs` te zien met daarin `rootfs/home/pi/video`. Rootlfs is maar 2,3 GB groot terwijl de SD-kaart 32 GB is. Rootfs groter maken met:

```bash
sudo raspi-config
```

In raspi-config naar 'Advanced options' > 'Expand filesystem'. Vervolgens een reboot om van de nieuwe grootte gebruik te kunnen maken.

Video bestand kopiëren met:

```bash
sudo cp '/path/source/file.mp4' '/rootfs/home/pi/video/file.mp4'
```


## Informatie

* Raspberry Pi Video Looper for Trade Shows
  * https://medium.com/@srborders1985/raspberry-pi-video-looper-for-trade-shows-c6c0cbc1ad01
* Raspberry Pi Video Looper
  * https://videolooper.de/
* pi_video_looper
  * https://github.com/adafruit/pi_video_looper
* Raspberry Pi Video Looper
  * https://learn.adafruit.com/raspberry-pi-video-looper


## Test met Piazza Trento e Trieste 20 keer

```bash
# repeat 20 times, 863 frames, video only
ffmpeg -i piazzatrentoetrieste-video-x1.mp4 -filter_complex "loop=loop=20:size=863:start=0" piazzatrentoetrieste-video-x20.mp4
# repeat 20 times, audio
ffmpeg -stream_loop 20 -i piazzatrentoetrieste-audio-x1.wav -c copy piazzatrentoetrieste-audio-x20.wav
# video en audio samenvoegen
ffmpeg -i piazzatrentoetrieste-video-x20.mp4 -i piazzatrentoetrieste-audio-x20.wav -vcodec copy piazzatrentoetrieste-x20.mp4
```

## Test met de video voor Beam On!, 23 november 2024

Video uit Screenflow her-encoderen met:

```bash
ffmpeg -i input.mp4 -f mp4 -vcodec libx264 -pix_fmt yuv420p output.mp4
```

De SD-kaart op een Linux computer, daar is `rootfs` te zien met daarin `rootfs/home/pi/video`.

```bash
# Vorige video weg met met:
sudo rm '/media/wouter/rootfs/home/pi/video/file.mp4'
# Video bestand kopiëren met:
sudo cp '/home/wouter/Desktop/file.mp4' '/media/wouter/rootfs/home/pi/video/file.mp4'
```

# LP173WD1-TLA1 / Raspberry Pi Zero

Een scherm dat ik op Marktplaats gekocht heb, een bijbehorend controller board gevonden op
AliExpress, en video spelend van een Raspberry Pi Zero.

## LP173WD1-TLA1 LCD screen

* 17.3"
* 1600 x 900
* Pin Pitch: 0.5 mm
* Pins: 40

![lp173wd1-tla1-backside](assets/images/lp173wd1-tla1-backside.jpg 'lp173wd1-tla1-backside')

## Controller board

* Controller Board Kit Voor LP173WD1-TLA1 LP173WD1-TLA2 LP173WD1-TLA3 Tv + Hdmi + Vga + Av + Usb Lcd Led Screen Driver board
* Verkoper: Yqwsyxl LCD parts Store
* https://nl.aliexpress.com/item/1005001377305217.html
* € 22,06

![Controller connections](assets/images/controller-board-voor-lp173wd1-tla1.jpg 'Controller connections')

* Earphone uitgang werkt goed.
* Audio minijack uitgang niet, is stil.

### Control panel

![Controlpanel](assets/images/lp173wd1-tla1-controlpanel.jpg 'Controlpanel')

[POWER] [MENU] [INPUT] [VOL-] [VOL+] [CH-] [CH+]

* Lcd-scherm Controller Board Fit LP173WD1-TLA1/TLC2/TLD3/TLE1 Hdmi-Compatibel Av Vga Lvds 40-Pin 17.3 "1600*900 Diy Kit
* Verkoper: LCD/LED driver board card kit-V04 Store
* https://nl.aliexpress.com/item/1005004846839242.html
* € 23,33

![lp173wd1-tla1-LVDS-40pin](assets/images/lp173wd1-tla1-LVDS-40pin.jpg 'lp173wd1-tla1-LVDS-40pin')

![lp173wd1-tla1-LVDS-40pin 2](assets/images/lp173wd1-tla1-LVDS-40pin-2.jpg 'lp173wd1-tla1-LVDS-40pin 2')

![lp173wd1-tla1-connected](assets/images/lp173wd1-tla1-connected.jpg 'lp173wd1-tla1-connected')

## Raspberry Pi Zero

Manual start:

* Connect to power adapter.
* Connect RPi Mini HDMI to controller HDMI.
* `ssh pi@raspberrypi.local`
* Password: raspberry
* `/etc/init.d/videoloop start`

* On controller choose HDMI as input

No signal from RPi.

Try Thinkpad HDMI out. No, has no HDMI out.

![lp173wd1-tla1-powered](assets/images/lp173wd1-tla1-powered.jpg 'lp173wd1-tla1-powered')

## Aangesloten

Het scherm met de controller en RPi aangesloten en op de ezel met punaises.

![lp173wd1-tla1-op-ezel](assets/images/lp173wd1-tla1-opstelling-op-ezel.jpg 'lp173wd1-tla1-op-ezel')

Het scherm met standaard instellingen, later ontdekt dat met de afstandbediening van de controller
in het menu de kleuren ingesteld kunnen worden. Staat hier op 'warm', maar 'standaard' is beter.

![lp173wd1-tla1-op-ezel](assets/images/lp173wd1-tla1-video-spelend.jpg 'LP173WD1-TLA1 video spelend')

Er blijkt ook aan de achterkant licht uit te komen.

![lp173wd1-tla1-achterlicht-1](assets/images/lp173wd1-tla1-achterlicht-1.jpg 'LP173WD1-TLA1 licht achterkant')

![lp173wd1-tla1-achterlicht-2](assets/images/lp173wd1-tla1-achterlicht-2.jpg 'LP173WD1-TLA1 licht achterkant')
